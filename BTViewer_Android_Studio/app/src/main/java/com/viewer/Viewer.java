package com.viewer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
import java.util.Vector;

import com.bluetooth.BluetoothConnection;
import com.flotandroidchart.flot.FlotChartContainer;
import com.flotandroidchart.flot.FlotDraw;
import com.flotandroidchart.flot.Options;
import com.flotandroidchart.flot.data.SeriesData;
import com.ringBuffer.RingBuffer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Viewer extends Activity {

	private Thread workerThread;
	volatile private boolean stopWorker;

	final private BluetoothConnection BTConnection = new BluetoothConnection();

	private Options options;
	private Vector<SeriesData> vSeriesData;
	private FlotChartContainer flotChartContainer;
	private FlotDraw flotDraw;

	private int indexPlotADC;
	private SeriesData seriesDataADC_1;
	private double[][] dataADC_1;
	private SeriesData seriesDataADC_2;
	private double[][] dataADC_2;
	private SeriesData seriesDataADC_3;
	private double[][] dataADC_3;
	private SeriesData seriesDataADC_4;
	private double[][] dataADC_4;

	private int indexPlotUARTPleth;
	private SeriesData seriesDataUARTPleth;
	private double[][] dataUARTPleth;

	private RingBuffer ringBuffer;

	private int nSample;

	private ToggleButton bluetoothButton;
	private Button synchButton;

	private String state;
	private Time now;
	private File rawFile;
	private BufferedOutputStream rawOutputStream;
	private FileWriter synchEventsFile;

	/*
	 * The tag has a 1 byte length (TAG = 0xB5)
	 * The ADC samples N-channels and each channel stores M-samples (uint16) so
	 * the size of the packet is 2*N*M, but we wrap half the buffer so 2*N*M/2
	 * The Nonin packet contains 6 bytes: 2 (Pleth) + 1 (SpO2) + 2 (Pleth) + 1 (SpO2)
	 */
	public static final byte PACKET_TAG            = (byte) 0xC0;
	public static final int ADC_CHANNELS           = 8;
	public static final int ADC_SIZE_CHANNEL       = 2;
	public static final int ADC_STATUS_BYTES       = 2; //Actually there are 3 status bytes, but 1 of them has been removed just to make pair the number of bytes
	public static final int ADC_PACKET_SIZE        = ADC_STATUS_BYTES + (ADC_CHANNELS*ADC_SIZE_CHANNEL);
	public static final int PACKET_SIZE            = ADC_PACKET_SIZE;
	public static final int MIN_RING_BUFFER_LENGTH = PACKET_SIZE + 1;

	/*
	 * Mode Nonin = {2, 7}
	 * Mode 2: 0x0 + 1 byte Pleth + 1 byte SpO2
	 * Mode 7: 1 byte Pleth (MSB) + 1 byte Pleth (LSB) + 1 byte SpO2
	 */
	public static final int NONIN_MODE = 2;  // Mode Nonin 2 or 7

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		options = new Options();
		options.fps = 50;
		options.yaxis.min = -100;
		options.yaxis.max = 4500;

		dataADC_1 = new double[1500][2];
		for (int i = 0; i < dataADC_1.length; i++) {
			dataADC_1[i][0] = i;
			dataADC_1[i][1] = 0.0d;
		}

		dataADC_2 = new double[1500][2];
		for (int i = 0; i < dataADC_2.length; i++) {
			dataADC_2[i][0] = i;
			dataADC_2[i][1] = 0.0d;
		}

		dataADC_3 = new double[1500][2];
		for (int i = 0; i < dataADC_3.length; i++) {
			dataADC_3[i][0] = i;
			dataADC_3[i][1] = 0.0d;
		}

		dataADC_4 = new double[1500][2];
		for (int i = 0; i < dataADC_4.length; i++) {
			dataADC_4[i][0] = i;
			dataADC_4[i][1] = 0.0d;
		}

		dataUARTPleth = new double[500][2];
		for (int i = 0; i < dataUARTPleth.length; i++) {
			dataUARTPleth[i][0] = 3*i;
			dataUARTPleth[i][1] = 0.0d;
		}

		seriesDataADC_1 = new SeriesData();
		seriesDataADC_1.setData(dataADC_1);
		seriesDataADC_1.label = "ADC 1 (señal interna)";
		seriesDataADC_1.series.color = 0x80000D;

		seriesDataADC_2 = new SeriesData();
		seriesDataADC_2.setData(dataADC_2);
		seriesDataADC_2.label = "ADC 2";
		seriesDataADC_2.series.color = 0x00800D;

		seriesDataADC_3 = new SeriesData();
		seriesDataADC_3.setData(dataADC_3);
		seriesDataADC_3.label = "ADC 3";
		seriesDataADC_3.series.color = 0x00400A;

		seriesDataADC_4 = new SeriesData();
		seriesDataADC_4.setData(dataADC_4);
		seriesDataADC_4.label = "ADC 4";
		seriesDataADC_4.series.color = 0x08100C;

		seriesDataUARTPleth = new SeriesData();
		seriesDataUARTPleth.setData(dataUARTPleth);
		seriesDataUARTPleth.label = "UART Pleth";
		seriesDataUARTPleth.series.color = 0x000080;

		vSeriesData = new Vector<SeriesData>();
		vSeriesData.add(seriesDataADC_1);
		vSeriesData.add(seriesDataADC_2);
		vSeriesData.add(seriesDataADC_3);
		vSeriesData.add(seriesDataADC_4);
		vSeriesData.add(seriesDataUARTPleth);

		flotDraw = new FlotDraw(vSeriesData, options, null);
		flotChartContainer = (FlotChartContainer) this.findViewById(R.id.flotDraw);
		if (flotChartContainer != null) {
			flotChartContainer.setDrawData(flotDraw);
		}

		ringBuffer = new RingBuffer();

		state = Environment.getExternalStorageState();
		now = new Time();
		now.setToNow();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File dirFile = new File(Environment.getExternalStorageDirectory() + "/BBB/");
			dirFile.mkdir();
			rawFile = new File(dirFile.toString() + "/" + now.format("%Y.%m.%d-%H.%M.%S") + "-raw.log");
			try {
				rawOutputStream = new BufferedOutputStream(new FileOutputStream(rawFile));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		nSample = 0;

		bluetoothButton = (ToggleButton) findViewById(R.id.BluetoothButton);

		// Button
		bluetoothButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					if (BTConnection.findDevice(Viewer.this)) {
						Toast.makeText(getApplicationContext(), "Bluetooth device found", Toast.LENGTH_SHORT).show();
						UUID uuid = UUID.fromString("34B1CF4D-1069-4AD6-89B6-E161D79BE4D8");
						if (BTConnection.open(uuid)) {
							Toast.makeText(getApplicationContext(), "Bluetooth device opened", Toast.LENGTH_LONG).show();
							beginListen();
						} else {
							bluetoothButton.setChecked(false);
							Toast.makeText(getApplicationContext(), "Bluetooth device not opened", Toast.LENGTH_LONG).show();
						}
					} else {
						bluetoothButton.setChecked(false);
						Toast.makeText(getApplicationContext(), "Bluetooth device not found", Toast.LENGTH_LONG).show();
					}
				} else {
					if (BTConnection.close()) {
						stopWorker = true;
						Toast.makeText(getApplicationContext(), "Bluetooth connection closed", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getApplicationContext(), "Bluetooth connection already closed", Toast.LENGTH_LONG).show();
					}
				}
			}
		});

		// Button: synchronization event
		synchButton = (Button) findViewById(R.id.synchButton);
		synchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (synchEventsFile == null) {
					if (Environment.MEDIA_MOUNTED.equals(state)) {
						File dirFile = new File(Environment.getExternalStorageDirectory() + "/BBB/");
						dirFile.mkdir();
						try {
							synchEventsFile = new FileWriter(dirFile.toString() + "/" +
									now.format("%Y.%m.%d-%H.%M.%S") + "-synchoEvents.txt");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				try {
					synchEventsFile.write("Evento de sincronizacion en muestra " + Integer.toString(nSample) + " de ADC\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchButton.setBackgroundColor(Color.RED);
				Toast.makeText(getApplicationContext(),
						"New synchro event introduced", Toast.LENGTH_SHORT).show();
			}
		});

	}

	void beginListen() {

		//final Handler handler = new Handler();

		stopWorker = false;

		workerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted() && !stopWorker) {

					if (BTConnection.receiveDataIsAvailable()) {

						byte[] recvBuff = BTConnection.receiveData();
						int bytesRead = BTConnection.getBytesRead();
						//int[] recvData = new int[bytesRead >> 1];

						for (int i = 0; i < bytesRead; i++) {
							ringBuffer.add(recvBuff[i]);
						}

						try {
							if (bytesRead > 0) {
								rawOutputStream.write(recvBuff, 0, bytesRead);
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					while (ringBuffer.size() > MIN_RING_BUFFER_LENGTH) {
						double point;
						//BTConnection.sendData(1);

						switch (ringBuffer.get()) {

							case PACKET_TAG:

								int byte_to_ignore = ringBuffer.get(); //2nd byte of status

								/*
								for (int j=0; j < 2; j++) { //We don't need these channels (for now we only print channel 1)
									ringBuffer.get();
								}
*/
								int first_byte = ringBuffer.get();
								int second_byte = ringBuffer.get();
								point = (first_byte | second_byte) + 3500;
								seriesDataADC_1.datapoints.points.set(2*indexPlotADC + 1, (double) point);



								int third_byte = (ringBuffer.get()) + 200 ;
								int fourth_byte = (ringBuffer.get()) + 200;
								point = (third_byte | fourth_byte) + 2500;
								seriesDataADC_2.datapoints.points.set(2*indexPlotADC + 1, (double) point);


								int fifth_byte = (ringBuffer.get()) + 200 ;
								int sixth_byte = (ringBuffer.get()) + 200;
								point = (fifth_byte | sixth_byte) + 1500;
								seriesDataADC_3.datapoints.points.set(2*indexPlotADC + 1, (double) point);

								int seventh_byte = (ringBuffer.get()) + 200 ;
								int eighth_byte = (ringBuffer.get()) + 200;
								point = (seventh_byte | eighth_byte) + 500;
								seriesDataADC_4.datapoints.points.set(2*indexPlotADC + 1, (double) point);

								indexPlotADC++;

								if (indexPlotADC == dataADC_1.length) {
									indexPlotADC = 0;
									//BTConnection.sendData(1);
								}

								if (indexPlotADC == dataADC_2.length) {
									indexPlotADC = 0;
									//BTConnection.sendData(1);
								}

								if (indexPlotADC == dataADC_3.length) {
									indexPlotADC = 0;
									//BTConnection.sendData(1);
								}

								if (indexPlotADC == dataADC_4.length) {
									indexPlotADC = 0;
									//BTConnection.sendData(1);
								}
								nSample++;

								for (int j=10; j < ADC_PACKET_SIZE; j++) { //We don't need these channels (for now we only print channel 1)
									ringBuffer.get();
								}

								/*
								for (int j = 0; j < ADC_PACKET_SIZE; j=j+4) {
									point = ((ringBuffer.get() & 0xFF) | ((ringBuffer.get() & 0xFF) << 8)) + 500;
									seriesDataADC_1.datapoints.points.set(2*indexPlotADC + 1, (double) point);
									point = ((ringBuffer.get() & 0xFF) | ((ringBuffer.get() & 0xFF) << 8)) + 1000;
									seriesDataADC_2.datapoints.points.set(2*indexPlotADC + 1, (double) point);
									indexPlotADC++;
									if (indexPlotADC == dataADC_1.length) {
										indexPlotADC = 0;
									}
									nSample++;
								}
								for (int j = 0; j < NONIN_PACKET_SIZE; j=j+3) {
									if (NONIN_MODE == 2) {
										ringBuffer.get();
										point = (ringBuffer.get() & 0xFF) * 10;
									} else if (NONIN_MODE == 7) {
										point = (((ringBuffer.get() & 0xFF) << 8) | (ringBuffer.get() & 0xFF)) / 16.0d;
									}
									seriesDataUARTPleth.datapoints.points.set(2*indexPlotUARTPleth + 1, (double) point);
									indexPlotUARTPleth++;
									if (indexPlotUARTPleth == dataUARTPleth.length) {
										indexPlotUARTPleth = 0;
									}
									ringBuffer.get();  //SpO2, no lo muestro aun
								}
								*/
								break;

							default:
								/*
								for (int j = 0; j < ADC_PACKET_SIZE; j=j+4) {
									int first_byte = ringBuffer.get();
									int first_byte_masked = (first_byte & 0xFF);
									int second_byte = ringBuffer.get();
									int second_byte_masked = ((second_byte & 0xFF) << 8);
									point = (first_byte_masked | second_byte_masked) + 500;
									seriesDataADC_1.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									indexPlotADC++;
									if (indexPlotADC == dataADC_1.length) {
										indexPlotADC = 0;
									}
									nSample++;
								}
								*/
								break;

						}
					}
				}
			}
		});

		workerThread.start();

	}

	@Override
	public void onStop() {
		super.onStop();
		if (synchEventsFile != null) {
			try {
				synchEventsFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (rawOutputStream != null) {
			try {
				rawOutputStream.flush();
				rawOutputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (BTConnection.close()) {
			stopWorker = true;
		}
	}

}
